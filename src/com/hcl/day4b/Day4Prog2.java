package com.hcl.day4b;

class Account{
			int accno;
			String custName;
			Account(int accno,String custName) {
						this.accno=accno;
						this.custName=custName;
			}
			void display() {
					System.out.println(accno);
					System.out.println(custName);
			}
}
class currentAccount extends Account {
			int currBal;
			currentAccount(int accno,String custName,int currBal) {
					super(accno,custName);
					this.currBal=currBal;
			}
			void display() {
					super.display();
					System.out.println(currBal);
			}
}
class accDetails extends currentAccount{
			int depositAmount,withdrawAmount;
			accDetails(int accno,String custName,int currBal,int depositAmount,int withdrawAmount) {
					super(accno,custName,currBal);
					this.depositAmount=depositAmount;
					this.withdrawAmount=withdrawAmount;
			}
			void display() {
					super.display();
					System.out.println(depositAmount);
					System.out.println(withdrawAmount);
			}
}

public class Day4Prog2 {

		public static void main(String[] args) {
					accDetails a1=new accDetails(11111,"Harshit",10000,5000,500);
					accDetails a2=new accDetails(13,"ajay",226,9796,2398);
					a1.display();
					a2.display();

	}

}
