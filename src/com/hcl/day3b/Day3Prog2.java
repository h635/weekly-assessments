package com.hcl.day3b;


class Vehicle {
	
	void noOfWheels() {
		
			System.out.println("No of wheels undifined");
	}
}
	class Scooter extends Vehicle{
			void noOfWheels() {
				
					System.out.println("No of wheels 2");
			}
	}
	class Car extends Vehicle{
			void noOfWheels() {
				System.out.println("No of wheels 4");
			}
	}

public class Day3Prog2 {

	public static void main(String[] args) {
		
		
		Vehicle ob=new Vehicle();
		ob.noOfWheels();
		Scooter ob1=new Scooter();
		ob1.noOfWheels();
		Car ob2 = new Car();
		ob2.noOfWheels();
		

	}

}
