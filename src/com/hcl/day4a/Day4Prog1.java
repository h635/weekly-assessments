package com.hcl.day4a;

interface Common {
		abstract String markAttendance();
		abstract String dailyTask();
		abstract String displayDetails();
}

class Employee implements Common {
		public String markAttendance() {
			return "Attendance marked for today";
		}
		public String dailyTask() {
			return "Complete coding of module 1";
		}
		public String displayDetails() {
			return "Employee details";
		}
}
class Manager implements Common {
			public String markAttendance() {
				return "Attendance marked for today";
			}		
			public String dailyTask() {
				return "Create project architecture";
			}
			public String displayDetails() {
				return "Manager details";
			}
			
}
	

public class Day4Prog1 {

		public static void main(String[] args) {
				Common i1=new Employee();
				Common i2=new Manager();
				System.out.println(i1.markAttendance());
				System.out.println(i1.dailyTask());
				System.out.println(i1.displayDetails());
				System.out.println(i2.markAttendance());
				System.out.println(i2.dailyTask());
				System.out.println(i2.displayDetails());
	}
		
}


